#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/*--------------------------huffmanTree-------------------------------
|  Cau Truc 	huffmanTree
|
|  Description:  	The structure of Huffman Binary Tree
*-------------------------------------------------------------------*/
struct huffmanTree {
    int symbol;
    int frequency;
    struct huffmanTree *leftChild;
    struct huffmanTree *rightChild;
};

/*--------------------------isLeaf-------------------------------
|  Name: 		isLeaf
|
|  Description:  	Check whether a node of huffman tree is a leaf?
|
|  Inputs:
|				struct huffmanTree *nodeToCheck
|
|
|  Results:  
|				1	- 	1 If node is a leaf
|				2	-	0 If node is not a leaf
*-------------------------------------------------------------------*/
int isLeaf(struct huffmanTree *nodeToCheck);

/*--------------------------getTotalNumberSymbols-------------------------------
|  Name: 		getTotalNumberSymbols
|
|  Description:  	Counting the total number of different symbols in the document.
|
|  Inputs:
|				int	tableFrequency[]
|               	The table frequency of 256 ASCII symbols which appear in the document.
|
|  Results:  
|				int The total number of different symbols.
*-------------------------------------------------------------------*/
int getTotalNumberSymbols(int tableFrequency[]);

/*--------------------------compressFile-------------------------------
|  Name: 		compressFile
|
|  Description:  	Compressing file by Huffman Coding Algorithm
|
|  Inputs:
|
|  Results:  
|				- 	Enter the file path
|				-	Calculate the frequency of symbols
|				- 	Build huffman tree
|				- 	Build encoded symbols
|				- 	Compress and write to file .huffman
*-------------------------------------------------------------------*/
void compressFile();

/*--------------------------decompressFile-------------------------------
|  Name: 		decompressFile
|
|  Description:  	Decompressing a Huffman Encoded file
|
|  Inputs:
|
|  Results:  
|				- 	Enter the file path with extension .huffman
|				-	Read the compressed file
|				- 	Build Huffman Tree
|				- 	Decompress and create file.
*-------------------------------------------------------------------*/
void decompressFile();

/*--------------------------buildHuffmanCode-------------------------
|  Name: 		buildHuffmanCode
|
|  Description:  	Build the encoded symbols from the huffman tree
|
|  Inputs:
|				struct huffmanTree *huffman
|				char 	encodingString[]
|				int 	index
|					The position of current encoded symbol inside the encoding string.
|
|  Results:  
|				1	- 	Update the array char tableCode[256]
*-------------------------------------------------------------------*/
void buildHuffmanCode(char **tableCode, struct huffmanTree *huffman, char encodingString[], int index);

/*--------------------------buildHuffmanTree-------------------------------
|  Name: 		buildHuffmanTree
|
|  Description:  	Build the Huffman Tree from table of frequency
|
|  Inputs:
|				char **tableCode[]
                    -   table of encoded symbols
|				int  tableFrequency[]
|               	-	table frequency
|				char encodingString[]
|				int index
|  Results:  
|				Update the huffmanTree *huffman
*-------------------------------------------------------------------*/
struct huffmanTree *buildHuffmanTree(int tableFrequency[]);

int main() {
    int functionChoice = 3;
    do {
        printf("\n |****************Huffman Compressing Program************|");
        printf("\n |                                                       |");
        printf("\n |              * Compressing A File             (1)     |");
        printf("\n |              * Decompressing A File           (2)     |");
        printf("\n |                                                       |");
        printf("\n |*******************************************************|\n");
        printf("\n Please select the function: ");
        fflush(stdin);
        scanf("%d", &functionChoice);
        if ((functionChoice == 1) || (functionChoice == 2)) break;
    }
    while (1);
    if (functionChoice == 1) {
        compressFile();
    }
    else {
        decompressFile();
    }
    printf("\n |**********************The End**************************|\n");
    return 0;
}

void compressFile() {
    clock_t startTime, endTime;
    char inputFile[1000], compressedFile[1000];
    FILE *fpInput, *fpCompressed;
    struct huffmanTree *huffman;
    int totalNumberBits = 0;
    int totalNumberBitsCompressed = 0;
    int countBits = 0;
    int encodedSymbol = 0;
    int c, i;
    int tableFrequency[256];
    char *tableCode[256];
    int sizeOfChar = sizeof(char);
    int sizeOfInt = sizeof(int);
    int isFileOfOnlyOneSymbol = 0;
    printf("\n The file path: ");
    fflush(stdin);
    scanf("%s", inputFile);
    fpInput = fopen(inputFile, "rb");

    if (fpInput == NULL) {
        printf("\n Can not open the file\n");
        return;
    }
    startTime = clock();
    printf("\n Start building table of frequency... \n");

    for (i = 0; i < 256; i++) tableFrequency[i] = 0;

    while ((c = fgetc(fpInput)) != EOF) tableFrequency[c]++;
    endTime = clock();
    printf("\n Finish building table of frequency in %f second(s)\n", (double) (endTime - startTime) / CLOCKS_PER_SEC);
    fclose(fpInput);

    //Check extension .huffman
    strcpy(compressedFile, inputFile);
    strcat(compressedFile, ".huffman");
    if (getTotalNumberSymbols(tableFrequency) > 1) {
        startTime = clock();
        printf("\n Start building the Huffman Tree... \n");
        huffman = buildHuffmanTree(tableFrequency);
        endTime = clock();
        printf("\n Finish building the Huffman Tree in %f second(s)\n", (double) (endTime - startTime) / CLOCKS_PER_SEC);

        startTime = clock();
        printf("\n Start building the table of encoded symbols... \n");
        char encodingString[256];
        buildHuffmanCode(tableCode, huffman, encodingString, 0);
        endTime = clock();
        printf("\n Finish building the table of encoded symbols in %f second(s)\n", (double) (endTime - startTime) / CLOCKS_PER_SEC);

        printf("\n Start compressing file... \n");
        startTime = clock();

        fpInput = fopen(inputFile, "rb");
        fpCompressed = fopen(compressedFile, "wb");

        for (i = 0; i < 256; i++) if (tableFrequency[i] > 0) totalNumberBitsCompressed += strlen(tableCode[i]) * tableFrequency[i];

        fwrite(&isFileOfOnlyOneSymbol, sizeOfInt, 1, fpCompressed);
        fwrite(&totalNumberBitsCompressed, sizeOfInt, 1, fpCompressed);
        fwrite(tableFrequency, sizeOfInt, 256, fpCompressed);

        while ((c = fgetc(fpInput)) != EOF) {
            totalNumberBits += 8;
            int leng = strlen(tableCode[c]);
            for (i = 0; i < leng; i++) {
                if (countBits == 8) {
                    fwrite(&encodedSymbol, sizeOfChar, 1, fpCompressed);
                    encodedSymbol = 0;
                    countBits = 0;
                    i--;
                }
                else {
                    int d = *(tableCode[c] + i) - '0';
                    encodedSymbol += d << ((8 - countBits) - 1);
                    countBits++;
                }
            }
        }

        if (countBits != 0) fwrite(&encodedSymbol, sizeOfChar, 1, fpCompressed);

        endTime = clock();
        printf("\n Finish compressing file in %f second(s)\n",
               (double) (endTime - startTime) / CLOCKS_PER_SEC);
        printf("\n Compression rate: %f \n", (float) totalNumberBitsCompressed / totalNumberBits);
        printf("\n Compressed file: %s \n", compressedFile);
        fclose(fpInput);
        fclose(fpCompressed);
    }
    else {
        printf("\n Start compressing file... \n");
        startTime = clock();
        fpCompressed = fopen(compressedFile, "wb");
        isFileOfOnlyOneSymbol = 1;
        fwrite(&isFileOfOnlyOneSymbol, sizeOfInt, 1, fpCompressed);
        int theOnlySymbol = 256;
        int frequency = 0;
        for (i = 0; i < 256; i++) {
            if (tableFrequency[i] > 0) {
                theOnlySymbol = i;
                frequency = tableFrequency[i];
                break;
            }
        }
        fwrite(&theOnlySymbol, sizeOfInt, 1, fpCompressed);
        fwrite(&frequency, sizeOfInt, 1, fpCompressed);
        fclose(fpCompressed);
        endTime = clock();
        printf("\n Finish compressing file in %f second(s)\n",
               (double) (endTime - startTime) / CLOCKS_PER_SEC);
        printf("\n Compression rate %f \n", (float) 1 / frequency);
        printf("\n Compressed file %s \n", compressedFile);
    }
}

void decompressFile() {
    char decompressedFile[1000], compressedFile[1000];
    FILE *fpCompressed, *fpDecompressed;
    int tableFrequency[256];
    int k, i, totalNumberBitsCompressed;
    clock_t startTime, endTime;
    int sizeOfChar = sizeof(char);
    int sizeOfInt = sizeof(int);
    int isFileOfOnlyOneSymbol = 0;

    printf("\n The file (.huffman) path: ");
    fflush(stdin);
    scanf("%s", compressedFile);
    if (strstr(compressedFile, ".huffman") == NULL) {
        printf("\n Not file with extension .huffman.\n");
        return;
    }
    fpCompressed = fopen(compressedFile, "rb");
    if (fpCompressed == NULL) {
        printf("\n Can not open file %s !\n", compressedFile);
        return;
    }
    int x = strlen(compressedFile) - 8;
    strncpy(decompressedFile, compressedFile, x);
    decompressedFile[x] = '\0';
    fpDecompressed = fopen(decompressedFile, "wb");

    fread(&isFileOfOnlyOneSymbol, sizeOfInt, 1, fpCompressed);
    if (isFileOfOnlyOneSymbol == 0) {
        startTime = clock();
        printf("\n Start building the Huffman Tree... \n");
        fread(&totalNumberBitsCompressed, sizeOfInt, 1, fpCompressed);
        fread(tableFrequency, sizeOfInt, 256, fpCompressed);

        struct huffmanTree *huffman;

        huffman = buildHuffmanTree(tableFrequency);
        endTime = clock();
        printf("\n Finish building the Huffman Tree in %f second(s)\n", (double) (endTime - startTime) / CLOCKS_PER_SEC);

        startTime = clock();
        printf("\n Start decompressing... \n");

        int countBits = 8;
        struct huffmanTree *tempNode = huffman;
        int encodedSymbol;
        int bitPosition;
        while (!feof(fpCompressed)) {
            fread(&encodedSymbol, sizeOfChar, 1, fpCompressed);

            if (totalNumberBitsCompressed < 8) countBits = totalNumberBitsCompressed;

            bitPosition = 8;

            for (k = countBits; k > 0; k--) {
                int bit = (encodedSymbol >> (bitPosition - 1)) & 1;

                if (bit == 0)
                    tempNode = tempNode->leftChild;
                else
                    tempNode = tempNode->rightChild;

                if (isLeaf(tempNode) == 1) {
                    fwrite(&(tempNode->symbol), sizeOfChar, 1, fpDecompressed);
                    tempNode = huffman;
                }
                bitPosition--;
                totalNumberBitsCompressed--;
            }
        }
        endTime = clock();
        printf("\n Finish decompressing %s in %f second(s)\n", decompressedFile,
               (double) (endTime - startTime) / CLOCKS_PER_SEC);
    }
    else {
        startTime = clock();
        printf("\n Start decompressing... \n");
        int theOnlySymbol;
        int frequency;
        fread(&theOnlySymbol, sizeOfInt, 1, fpCompressed);
        fread(&frequency, sizeOfInt, 1, fpCompressed);
        for (k = 0; k < frequency; k++) fwrite(&theOnlySymbol, sizeOfChar, 1, fpDecompressed);
        endTime = clock();
        printf("\n Finish decompressing %s in %f second(s)\n", decompressedFile,
               (double) (endTime - startTime) / CLOCKS_PER_SEC);
    }
    fclose(fpDecompressed);
    fclose(fpCompressed);
}

void buildHuffmanCode(char **tableCode, struct huffmanTree *huffman, char encodingString[], int index) {

    if (huffman->leftChild != 0) {
        encodingString[index] = '0';
        buildHuffmanCode(tableCode, huffman->leftChild, encodingString, index + 1);
    }

    if (huffman->rightChild != 0) {
        encodingString[index] = '1';
        buildHuffmanCode(tableCode, huffman->rightChild, encodingString, index + 1);
    }

    if (isLeaf(huffman) == 1) {
        int i;
        tableCode[huffman->symbol] = (char *) malloc(sizeof(char) * (index + 1));
        for (i = 0; i < index; i++) *(tableCode[huffman->symbol] + i) = encodingString[i];
        *(tableCode[huffman->symbol] + index) = '\0';
    }
}

struct huffmanTree *buildHuffmanTree(int tableFrequency[]) {
    int i, k;
    int n = getTotalNumberSymbols(tableFrequency);
    struct huffmanTree *theHuffmanTree[n];
    k = 0;

    for (i = 0; i < 256; i++) {
        if (tableFrequency[i] > 0) {
            theHuffmanTree[k] = (struct huffmanTree *) malloc(sizeof(struct huffmanTree));
            theHuffmanTree[k]->leftChild = 0;
            theHuffmanTree[k]->rightChild = 0;
            theHuffmanTree[k]->frequency = tableFrequency[i];
            theHuffmanTree[k]->symbol = i;
            k++;
        }
    }

    while (n > 1) {
        int max;
        for (i = 0; i < (n - 1); i++) {
            max = i;
            for (k = (i + 1); k < n; k++)
                if (theHuffmanTree[k]->frequency > theHuffmanTree[max]->frequency)
                    max = k;

            if (i != max) {
                int symbol = theHuffmanTree[i]->symbol;
                int frequency = theHuffmanTree[i]->frequency;
                struct huffmanTree *leftChild = theHuffmanTree[i]->leftChild;
                struct huffmanTree *rightChild = theHuffmanTree[i]->rightChild;

                theHuffmanTree[i]->symbol = theHuffmanTree[max]->symbol;
                theHuffmanTree[i]->frequency = theHuffmanTree[max]->frequency;
                theHuffmanTree[i]->leftChild = theHuffmanTree[max]->leftChild;
                theHuffmanTree[i]->rightChild = theHuffmanTree[max]->rightChild;

                theHuffmanTree[max]->leftChild = leftChild;
                theHuffmanTree[max]->rightChild = rightChild;
                theHuffmanTree[max]->symbol = symbol;
                theHuffmanTree[max]->frequency = frequency;
            }
        }
        struct huffmanTree *leftBranch = theHuffmanTree[i]->leftChild;
        struct huffmanTree *rightBranch = theHuffmanTree[i]->rightChild;
        rightBranch = (struct huffmanTree *) malloc(sizeof(struct huffmanTree));

        rightBranch->leftChild = theHuffmanTree[n - 1]->leftChild;
        rightBranch->rightChild = theHuffmanTree[n - 1]->rightChild;
        rightBranch->symbol = theHuffmanTree[n - 1]->symbol;
        rightBranch->frequency = theHuffmanTree[n - 1]->frequency;
        free(theHuffmanTree[n - 1]);

        leftBranch = (struct huffmanTree *) malloc(sizeof(struct huffmanTree));
        leftBranch->leftChild = theHuffmanTree[n - 2]->leftChild;
        leftBranch->rightChild = theHuffmanTree[n - 2]->rightChild;
        leftBranch->symbol = theHuffmanTree[n - 2]->symbol;
        leftBranch->frequency = theHuffmanTree[n - 2]->frequency;

        theHuffmanTree[n - 2]->leftChild = leftBranch;
        theHuffmanTree[n - 2]->rightChild = rightBranch;
        theHuffmanTree[n - 2]->symbol = 256;
        theHuffmanTree[n - 2]->frequency = leftBranch->frequency + rightBranch->frequency;

        n--;
    }

    return theHuffmanTree[0];
}

int getTotalNumberSymbols(int tableFrequency[]) {
    int count = 0;
    int i;
    for (i = 0; i < 256; i++) if (tableFrequency[i] > 0) count++;
    return count;
}

int isLeaf(struct huffmanTree *nodeToCheck) {
    return (nodeToCheck->leftChild == 0 && nodeToCheck->rightChild == 0) ? 1 : 0;
}
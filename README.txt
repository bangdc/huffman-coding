Huffman Encoding

About
-----

Implemented the Huffman Coding algorithm for compression of file.
https://en.wikipedia.org/wiki/Huffman_coding

Usage
-----
With Terminal on linux machine

gcc -o program huffman_coding.c
./program